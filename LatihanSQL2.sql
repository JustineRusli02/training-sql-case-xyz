CREATE DATABASE Latihan
USE Latihan

CREATE TABLE Videos(
	VideoId int IDENTITY(0,1) PRIMARY KEY,
	VideoName varchar(255),
	CreateAt DATETIMEOFFSET DEFAULT GetDate(),
	CreatedBy varchar(255) DEFAULT 'SYSTEM',
	UpdateAt DATETIMEOFFSET DEFAULT GetDate(),
	UpdatedBy varchar(255) DEFAULT 'SYSTEM',
)

CREATE TABLE Advertisement(
	AdvertisementId int IDENTITY(0,1) PRIMARY KEY,
	AdvertisementName varchar(255),
	CreateAt DATETIMEOFFSET DEFAULT GetDate(),
	CreatedBy varchar(255) DEFAULT 'SYSTEM',
	UpdateAt DATETIMEOFFSET DEFAULT GetDate(),
	UpdatedBy varchar(255) DEFAULT 'SYSTEM',
)

CREATE TABLE TempVideoAdvertisement(
	TempId int IDENTITY(0,1) PRIMARY KEY,
	AdvertisementId int FOREIGN KEY REFERENCES Advertisement(AdvertisementId),
	VideoId int FOREIGN KEY REFERENCES Videos(VideoId),

)


CREATE TABLE News(
	NewsId int IDENTITY(0,1) PRIMARY KEY,
	NewsName varchar(255),
	CreateAt DATETIMEOFFSET DEFAULT GetDate(),
	CreatedBy varchar(255) DEFAULT 'SYSTEM',
	UpdateAt DATETIMEOFFSET DEFAULT GetDate(),
	UpdatedBy varchar(255) DEFAULT 'SYSTEM',

)

CREATE TABLE Customer(
	CustomerId int IDENTITY(0,1) PRIMARY KEY,
	VideoId int FOREIGN KEY REFERENCES Videos(VideoId),
	NewsId int FOREIGN KEY REFERENCES News(NewsId),
	CustomerName varchar(255) NOT NULL,
	CustomerStatus int NOT NULL, -- 0-> biasa    1-> banned
	CustomerMembership int NOT NULL, -- 0-> Free   1-> Premium
	CustomerEmail varchar(255) NOT NULL,
		
)

CREATE TABLE Forum(
	ForumId int IDENTITY(0,1) PRIMARY KEY,
	CustomerId int FOREIGN KEY REFERENCES Customer(CustomerId),
	ForumTopic varchar(255),
	ForumDescription varchar(255),
	CreateAt DATETIMEOFFSET DEFAULT GetDate(),
	CreatedBy varchar(255) DEFAULT 'SYSTEM',
	UpdateAt DATETIMEOFFSET DEFAULT GetDate(),
	UpdatedBy varchar(255) DEFAULT 'SYSTEM',

)

CREATE TABLE ReportCategory(
	ReportId int IDENTITY(0,1) PRIMARY KEY,
	ForumId int FOREIGN KEY REFERENCES Forum(ForumId),
	ReportDescription varchar(255),
	CreateAt DATETIMEOFFSET DEFAULT GetDate(),
	CreatedBy varchar(255) DEFAULT 'SYSTEM',
	UpdateAt DATETIMEOFFSET DEFAULT GetDate(),
	UpdatedBy varchar(255) DEFAULT 'SYSTEM',
)


CREATE TABLE Admin(
	AdminId int IDENTITY(0,1) PRIMARY KEY,
	AdvertisementId int FOREIGN KEY REFERENCES Advertisement(AdvertisementId),
	CustomerId int FOREIGN KEY REFERENCES Customer(CustomerId),
	ReportId int FOREIGN KEY REFERENCES ReportCategory(ReportId),
	AdminName varchar(255),
	AdminEmail varchar(255)
)
